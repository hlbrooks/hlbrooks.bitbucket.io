var searchData=
[
  ['delta_5fcount_290',['delta_count',['../classEncoderDriver_1_1EncoderDriver.html#a6cf2b4ee43f1f36e4e29e9f91932329a',1,'EncoderDriver.EncoderDriver.delta_count()'],['../classEncoderDriver__TP_1_1EncoderDriver.html#afffc6fc69c2524ad5bf42a155a01da88',1,'EncoderDriver_TP.EncoderDriver.delta_count()']]],
  ['delta_5fposition_291',['delta_position',['../classEncoderDriver_1_1EncoderDriver.html#adaf4a0f12acd267f5253e004711186d6',1,'EncoderDriver.EncoderDriver.delta_position()'],['../classEncoderDriver__TP_1_1EncoderDriver.html#a330e6635281021bc61d2ed06e6ece38d',1,'EncoderDriver_TP.EncoderDriver.delta_position()']]],
  ['delta_5fx_292',['delta_x',['../classposition__TP_1_1position.html#a8e6f857fe06db746a2891b22847de5c7',1,'position_TP::position']]],
  ['delta_5fy_293',['delta_y',['../classposition__TP_1_1position.html#a94aef83c4fa49a9a9ffe2cb1810916ca',1,'position_TP::position']]],
  ['dimes_294',['dimes',['../lab0x01_8py.html#a8ea6ab8b4ee6ad8c7903ea202a85d109',1,'lab0x01']]],
  ['drpupper_295',['DrPupper',['../lab0x01_8py.html#aea9dfb0c629dd0f15a46eb572a2b0eae',1,'lab0x01']]],
  ['duty_296',['duty',['../classMotorDriver_1_1MotorDriver.html#ac5b1e7813ea1d7fd4a71ca24d5515f36',1,'MotorDriver.MotorDriver.duty()'],['../classMotorDriver__TP_1_1MotorDriver.html#aeba5c652d36cd6e7d05507072ff92ed6',1,'MotorDriver_TP.MotorDriver.duty()']]]
];

/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Hunter's Portfolio", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab 0 Documentation", "index.html#sec_lab0", null ],
    [ "Lab 1 Documentation", "index.html#sec_lab1", null ],
    [ "Lab 2 Documentation", "index.html#sec_lab2", null ],
    [ "Lab 3 Documentation", "index.html#sec_lab3", null ],
    [ "Lab 4 Documentation", "index.html#sec_lab4", null ],
    [ "Lab 5 Documentation", "index.html#sec_lab5", null ],
    [ "Lab 6 Documentation", "index.html#sec_lab6", null ],
    [ "Lab 7 Documentation", "index.html#sec_lab7", null ],
    [ "Lab 8 Documentation", "index.html#sec_lab8", null ],
    [ "Term Project Documentation", "index.html#sec_TermProject", null ],
    [ "Lab 5 Hand Calcs", "lab_5HandCalcs.html", null ],
    [ "Lab 6 Simulation", "lab_6Simulation.html", null ],
    [ "Lab 7 Testing Procedures", "lab_7Testing.html", null ],
    [ "Term Project Calculations", "TermProjectCalcs.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Controller__TP_8py.html",
"lab0x01_8py.html#ab7819da9f0dc0d147443ac61e42d07cb"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
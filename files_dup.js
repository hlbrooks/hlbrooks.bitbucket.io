var files_dup =
[
    [ "Controller_TP.py", "Controller__TP_8py.html", "Controller__TP_8py" ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", "EncoderDriver_8py" ],
    [ "EncoderDriver_TP.py", "EncoderDriver__TP_8py.html", "EncoderDriver__TP_8py" ],
    [ "lab0x01.py", "lab0x01_8py.html", "lab0x01_8py" ],
    [ "lab0x02.py", "lab0x02_8py.html", "lab0x02_8py" ],
    [ "lab0x03.py", "lab0x03_8py.html", "lab0x03_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_TP.py", "main__TP_8py.html", "main__TP_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "me405lab4_main.py", "me405lab4__main_8py.html", "me405lab4__main_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "MotorDriver_TP.py", "MotorDriver__TP_8py.html", "MotorDriver__TP_8py" ],
    [ "my_script.py", "my__script_8py.html", null ],
    [ "position.py", "position_8py.html", "position_8py" ],
    [ "position_TP.py", "position__TP_8py.html", "position__TP_8py" ]
];